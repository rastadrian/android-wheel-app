# Android Wheel App
An "Android Wheel" Demo Application.

This application show's a very basic implementation of the Android-Wheel library for a Date Picker. It is the project from the [Android Wheel Guide](http://tolkianaa.blogspot.com/2016/01/how-to-use-android-wheel-v2-android.html).

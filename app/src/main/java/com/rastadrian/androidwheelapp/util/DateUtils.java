package com.rastadrian.androidwheelapp.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A Date utilities class.
 * Created on 1/7/16.
 *
 * @author Adrian Pena
 */
public class DateUtils {

    /**
     * Given a date and the range of days ahead, returns a list of the consecutive days.
     *
     * @param originalDate the original date
     * @param days the number of days' dates to add.
     * @return the list of consecutive dates.
     */
    public static List<Date> getNextNumberOfDays(Date originalDate, int days){
        List<Date> dates = new ArrayList<>();
        long offset;
        for(int i= 0; i<= days; i++){
            offset = 86400 * 1000L * i;
            Date date = new Date( originalDate.getTime()+offset);
            dates.add(date);
        }
        return dates;
    }
}

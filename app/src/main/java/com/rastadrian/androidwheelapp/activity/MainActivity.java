package com.rastadrian.androidwheelapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.rastadrian.androidwheelapp.R;
import com.rastadrian.androidwheelapp.fragment.DatePickerDialogFragment;

/**
 * Application's only activity, it provides a button to engage the date picker.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showPickerButton = (Button) findViewById(R.id.btn_show_picker);
        showPickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialogFragment().show(getSupportFragmentManager(), DatePickerDialogFragment.class.getSimpleName());
            }
        });
    }
}

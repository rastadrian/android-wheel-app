package com.rastadrian.androidwheelapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rastadrian.androidwheelapp.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;

/**
 * Wheel Adapter for the Day spinner of the Date Picker Wheel.
 * Created on 1/7/16.
 *
 * @author Adrian Pena
 */
public class DayWheelAdapter extends AbstractWheelTextAdapter {

    private final List<Date> mDateList;

    public DayWheelAdapter(Context context, List<Date> dateList) {
        super(context, R.layout.wheel_item_time);
        this.mDateList = dateList;
    }

    @Override
    public View getItem(int index, View convertView, ViewGroup parent) {
        View view = super.getItem(index, convertView, parent);
        TextView weekday = (TextView) view.findViewById(R.id.time_item);

        //Format the date (Name of the day / number of the day)
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd", Locale.getDefault());
        //Assign the text
        weekday.setText(dateFormat.format(mDateList.get(index)));

        if (index == 0) {
            //If it is the first date of the array, set the color blue
            weekday.setText(R.string.today);
            weekday.setTextColor(Color.BLUE);
        }
        else{
            //If not set the color to black
            weekday.setTextColor(Color.BLACK);
        }

        return view;
    }

    @Override
    protected CharSequence getItemText(int i) {
        return "";
    }

    @Override
    public int getItemsCount() {
        if(mDateList != null) {
            return mDateList.size();
        }
        return 0;
    }
}

package com.rastadrian.androidwheelapp.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.rastadrian.androidwheelapp.R;
import com.rastadrian.androidwheelapp.adapter.DayWheelAdapter;
import com.rastadrian.androidwheelapp.util.DateUtils;

import java.util.Date;
import java.util.List;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;

/**
 * This dialog fragment provides the date picker implementation using Android-wheel.
 * Created on 1/7/16.
 *
 * @author Adrian Pena
 */
public class DatePickerDialogFragment extends DialogFragment {

    private static final String[] ARRAY_AM_PM = new String[] { "am" , "pm" };

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //inflate first.
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_date, null);

        //create the dialog second.
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setPositiveButton(R.string.button_set, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //NOP
                    }
                })
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //NOP
                    }
                })
                .setView(dialogView)
                .setTitle(getActivity().getString(R.string.dialog_title))
                .create();
        setupViews(dialogView);
        return alertDialog;
    }

    private void setupViews(View dialogView) {
        //With a custom method I get the next following 10 days from now
        List<Date> days = DateUtils.getNextNumberOfDays(new Date(), 10);

        //Configure Days Column
        WheelView day = (WheelView) dialogView.findViewById(R.id.wv_day);
        day.setViewAdapter(new DayWheelAdapter(getActivity(), days));

        //Configure Hours Column
        WheelView hour = (WheelView) dialogView.findViewById(R.id.wv_hour);
        NumericWheelAdapter hourAdapter = new NumericWheelAdapter(getActivity(), 1, 12);
        hourAdapter.setItemResource(R.layout.wheel_item_time);
        hourAdapter.setItemTextResource(R.id.time_item);
        hour.setViewAdapter(hourAdapter);

        //Configure Minutes Column
        WheelView min = (WheelView) dialogView.findViewById(R.id.wv_minute);
        NumericWheelAdapter minAdapter = new NumericWheelAdapter(getActivity(), 0, 59);
        minAdapter.setItemResource(R.layout.wheel_item_time);
        minAdapter.setItemTextResource(R.id.time_item);
        min.setViewAdapter(minAdapter);

        //Configure am/pm Marker Column
        WheelView ampm = (WheelView) dialogView.findViewById(R.id.wv_ampm);
        ArrayWheelAdapter<String> ampmAdapter = new ArrayWheelAdapter<>(getActivity(), ARRAY_AM_PM);
        ampmAdapter.setItemResource(R.layout.wheel_item_time);
        ampmAdapter.setItemTextResource(R.id.time_item);
        ampm.setViewAdapter(ampmAdapter);
    }
}
